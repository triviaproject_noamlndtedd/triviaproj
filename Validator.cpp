#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool boo = password.length() >= 4;
	if (boo)
	{
		boo = password.find(' ') == string::npos;
	}
	if (boo)
	{
		boo = password.find_first_of(DIGITS) != string::npos;
	}
	if (boo)
	{
		boo = password.find_first_of(UPPER_CASE) != string::npos;
	}
	if (boo)
	{
		boo = password.find_first_of(LOWER_CASE) != string::npos;
	}
	return boo;
}

bool Validator::isUsernameValid(string username)
{
	bool boo = username.length() > 0;
	if (boo)
	{
		boo = username.find(' ') == string::npos;
	}
	if (boo)
	{
		boo = (('z' >= username[0] && username[0] >= 'a') || ('Z' >= username[0] && username[0] >= 'A'));
	}
	return boo;
}