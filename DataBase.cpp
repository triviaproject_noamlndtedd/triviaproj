#include "DataBase.h"

string _count;
vector<Question*> questions;
vector<string> scores; // BestScores and personalScore status

DataBase::DataBase()
{
	stringstream sstr;
	if (sqlite3_open("trivia.db", &_db))
	{
		sqlite3_close(_db);
		sstr << "Can't open database: " << sqlite3_errmsg(_db);
		throw exception(sstr.str().c_str());
		sstr.clear();
	}
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::isUserExist(string username)
{
	int rc;
	bool exist;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "select count(*) from t_users where userName == '" << username << "';";

	rc = sqlite3_exec(_db, sstr.str().c_str(), callbackCount, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	exist = (_count != "0");
	return exist;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	int rc;
	bool boo = true;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "insert into t_users values ('" << username << "','" << password << "','" << email << "');";

	rc = sqlite3_exec(_db, sstr.str().c_str(), NULL, 0, &zErrMsg);
	if (sqlite3_exec(_db, sstr.str().c_str(), NULL, 0, &zErrMsg) != SQLITE_OK)
	{
		boo = false;
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return boo;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	int rc;
	bool boo;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "select count(*) from t_users where userName == '" << username << "' and password == '" << password << "';";

	rc = sqlite3_exec(_db, sstr.str().c_str(), callbackCount, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	boo = (_count != "0");
	return boo;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	int rc;
	stringstream sstr, error;
	char* zErrMsg;

	error << "SQL error: ";
	sstr << "select * from t_questions order by abs(random()) limit " << questionsNo << ";";
	questions.clear();

	rc = sqlite3_exec(_db, sstr.str().c_str(), callbackQuestions, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return questions;
}

vector<string> DataBase::getBestScores()
{
	int rc;
	stringstream sstr, error;
	char* zErrMsg;

	error << "SQL error: ";
	sstr << "select username,max(win_count) as max_count from (select username,count(*) as win_count from t_players_answers where is_correct == 1 group by username) group by username order by max_count desc limit 3;";
	scores.clear();

	rc = sqlite3_exec(_db, sstr.str().c_str(), callbackBestScores, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return scores;
}

int DataBase::insertNewGame()
{
	int rc;
	stringstream sstr, error;
	char* zErrMsg;
	int id;

	error << "SQL error: ";
	sstr << "insert into t_games(status, start_time, end_time) values(0, datetime('now'), NULL);";

	rc = sqlite3_exec(_db, sstr.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		id = -1;
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	else
		id = sqlite3_last_insert_rowid(_db);
	return id;
}

bool DataBase::updateGameStatus(int id)
{
	int rc;
	bool boo = true;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "update t_games set status = 0, end_time = datetime('now') where game_id == " << id << ";";

	rc = sqlite3_exec(_db, sstr.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		boo = false;
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return boo;
}

bool DataBase::addAnswerToPlayer(int gameID, string username, int questionID, string answer, bool isCorrect, int answerTime)
{
	int rc;
	bool boo = true;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "insert into t_players_answers values(" << gameID << ",'" << username << "'," << questionID << ",'" << answer << "'," << (isCorrect ? 1 : 0) << "," << answerTime << ");";

	rc = sqlite3_exec(_db, sstr.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		boo = false;
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return boo;
}

vector<string> DataBase::getPersonalStatus(string username)
{
	int rc;
	char* zErrMsg;
	stringstream sstr, error;

	error << "SQL error: ";
	sstr << "select count(distinct game_id), sum(is_correct), count(*) - sum(is_correct), avg(answer_time) from t_players_answers where username='" << username << "'";
	scores.clear();

	rc = sqlite3_exec(_db, sstr.str().c_str(), callbackPersonalStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		error << zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(error.str().c_str());
	}
	return scores;
}


int DataBase::callbackCount(void * notUsed, int argc, char ** argv, char ** azCol)
{
	if (argc > 0)
		_count = argv[0];
	return 0;
}

int DataBase::callbackQuestions(void * notUsed, int argc, char ** argv, char ** azCol)
{
	Question* currQuestion = nullptr;
	currQuestion = new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]);
	if (currQuestion)
		questions.push_back(currQuestion);
	return 0;
}

int DataBase::callbackBestScores(void * notUsed, int argc, char ** argv, char ** azCol)
{
	string currScore;
	currScore = Helper::getPaddedNumber(string(argv[0]).length(), 2);
	currScore += argv[0];
	currScore += Helper::getPaddedNumber(atoi(argv[1]), 6);
	scores.push_back(currScore);
	return 0;
}

int DataBase::callbackPersonalStatus(void * notUsed, int argc, char ** argv, char ** azCol)
{
	stringstream sstr;
	if (argv[3] == nullptr)
		sstr << /*number of games*/"0000" << /*number of right answers*/"000000" << /*number of wrong answers*/"000000" << /*average time for answer*/"0000";
	else
	{
		string s = argv[3];
		sstr << Helper::getPaddedNumber(atoi(argv[0]), 4) << Helper::getPaddedNumber(atoi(argv[1]), 6) << Helper::getPaddedNumber(atoi(argv[2]), 6) << Helper::getPaddedNumber(atoi(s.substr(0, s.find('.')).c_str()), 2) << Helper::getPaddedNumber(atoi(s.substr(s.find('.') + 1, s.find('.') + 1).c_str()), 2);
	}
	scores.push_back(sstr.str());
	return 0;
}