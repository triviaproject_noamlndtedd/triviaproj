#include "TriviaServer.h"

static const unsigned int IFACE = 0;

TriviaServer::TriviaServer() : _db()
{
	stringstream error;
	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
	{
		error << "WSAStartup error:" << WSAGetLastError();
		throw std::exception(error.str().c_str());
	}
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket < 0)
	{
		error << "ERROR opening socket:" << WSAGetLastError();
		throw std::exception(error.str().c_str());
	}
	TRACE("starting");
}

TriviaServer::~TriviaServer()
{
	closesocket(_socket);
	WSACleanup();
	_roomsList.clear();
	_connectedUsers.clear();
}

void TriviaServer::server()
{
	try
	{
		bindAndListen();
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	std::thread t1(&TriviaServer::handleRecivedMessages, this);

	t1.detach();

	while (true)
	{
		TRACE("accepting client...");
		TriviaServer::accept();
	}
}

void TriviaServer::bindAndListen()
{
	stringstream error;
	struct  sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;

	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == -1)
	{
		error << "error_with_binding:" << WSAGetLastError();
		throw std::exception(error.str().c_str());
	}
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == -1)
	{
		error << "listen error:" << WSAGetLastError();
		throw std::exception(error.str().c_str());
	}
	TRACE("listening on port %d...", PORT);
}

void TriviaServer::accept()
{
	SOCKET acceptSocket = ::accept(_socket, NULL, NULL);

	if (acceptSocket == INVALID_SOCKET)
	{
		TRACE("error accepting client");
	}
	else
	{
		TRACE("client accepted. client's socket- %d", acceptSocket);
		std::thread t2(&TriviaServer::clientHandler, this, acceptSocket);
		t2.detach();
	}
}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	int msgCode = Helper::getMessageTypeCode(client_socket);
	try
	{
		while (msgCode != 0 || msgCode != CtS_USER_EXIT)
		{
			addRecievedMessage(buildRecieveMessage(client_socket, msgCode));
			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		addRecievedMessage(buildRecieveMessage(client_socket, CtS_USER_EXIT));
	}
	catch (exception e)
	{
		cout << e.what() << endl;
		addRecievedMessage(buildRecieveMessage(client_socket, CtS_USER_EXIT));
	}
}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	lock_guard<std::mutex> lck(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	cv.notify_all();
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	std::vector<string> values;

	if (msgCode == CtS_SIN_IN)
	{
		//userName
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
		//password
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
	}
	else if (msgCode == CtS_SIN_UP)
	{
		//userName
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
		//password
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
		//email
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
	}
	else if (msgCode == CtS_USERS_IN_ROO)
		//roomID
		values.push_back(Helper::getStringPartFromSocket(client_socket, 4));

	else if (msgCode == CtS_JOIN_EXIS_ROO)
		//roomID
		values.push_back(Helper::getStringPartFromSocket(client_socket, 4));

	else if (msgCode == CtS_CRE_NEW_ROO)
	{
		//roomName
		values.push_back(Helper::getStringPartFromSocket(client_socket, Helper::getIntPartFromSocket(client_socket, 2)));
		//playersNumber 
		values.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		//questionsNumber 
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		//questionTimeInSec
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
	}
	else if (msgCode == CtS_ANS)
	{
		//answerNumber 
		values.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		//TimeInSeconds
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
	}

	return (new RecievedMessage(client_socket, msgCode, values));
}

void TriviaServer::handleRecivedMessages()
{
	User* user;
	RecievedMessage* msg;
	unique_lock<mutex> lck(_mtxRecievedMessages, std::defer_lock);
	while (true)
	{
		lck.lock();
		while (_queRcvMessages.empty())
		{
			cv.wait(lck);
		}
		msg = _queRcvMessages.front();
		_queRcvMessages.pop();
		lck.unlock();

		user = getUserBySocket(msg->getSock());
		msg->setUser(user);
		TRACE("handleRecivedMessages: msg code = %d, client_socket = %d", msg->getMessageCode(), msg->getSock());

		try
		{
			if (msg->getMessageCode() == CtS_SIN_IN)
			{
				handleSignIn(msg);
			}
			else if (msg->getMessageCode() == CtS_SIN_UP)
			{
				handleSignUp(msg);
			}
			else if (msg->getMessageCode() == CtS_SIN_OUT)
			{
				handleSignOut(msg);
			}
			else if (msg->getMessageCode() == CtS_LEAV_GAM)
			{
				handleLeaveGame(msg);
			}
			else if (msg->getMessageCode() == CtS_START_GAM)
			{
				handleStartGame(msg);
			}
			else if (msg->getMessageCode() == CtS_ANS)
			{
				handlePlayerAnswer(msg);
			}
			else if (msg->getMessageCode() == CtS_CRE_NEW_ROO)
			{
				handleCreateRoom(msg);
			}
			else if (msg->getMessageCode() == CtS_CLOS_ROO)
			{
				handleCloseRoom(msg);
			}
			else if (msg->getMessageCode() == CtS_JOIN_EXIS_ROO)
			{
				handleJoinRoom(msg);
			}
			else if (msg->getMessageCode() == CtS_LEAV_ROO)
			{
				handleLeaveRoom(msg);
			}
			else if (msg->getMessageCode() == CtS_USERS_IN_ROO)
			{
				handleGetUsersInRoom(msg);
			}
			else if (msg->getMessageCode() == CtS_GET_EXIS_ROOMS)
			{
				handleGetRooms(msg);
			}
			else if (msg->getMessageCode() == CtS_BEST_SCORE)
			{
				handleGetBestScores(msg);
			}
			else if (msg->getMessageCode() == CtS_STAT)
			{
				handleGetPersonalStatus(msg);
			}
			else
			{
				safeDeleteUesr(msg);
			}
		}
		catch (exception e)
		{
			safeDeleteUesr(msg);
			cout << e.what() << endl;
		}
	}
}

bool TriviaServer::handleSignUp(RecievedMessage* msg)
{
	if (!Validator::isPasswordValid(msg->getValues()[1]))
	{
		Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_UP_PASS_ILG);
		return false;
	}
	else if (!Validator::isUsernameValid(msg->getValues()[0]))
	{
		Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_UP_USERN_ILG);
		return false;
	}
	else if (_db.isUserExist(msg->getValues()[0]))
	{
		Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_UP_USERN_EXIS);
	}
	else
	{
		if (_db.addNewUser(msg->getValues()[0], msg->getValues()[1], msg->getValues()[2]))
		{
			Helper::sendData(msg->getSock(), StC_SUCC_SIN_UP);
			return true;
		}
		else
		{
			Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_UP_ELSE);
		}
	}
	Helper::sendData(msg->getSock(), StC_SUCC_SIN_UP);
	return true;
}


User* TriviaServer::handleSignIn(RecievedMessage* msg)
{
	User* user = nullptr;


	if (!_db.isUserAndPassMatch(msg->getValues()[0], msg->getValues()[1]))
		Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_IN_WRNG_DTLS);
	else if (getUserByName(msg->getValues()[0]) != NULL)
	{
		Helper::sendData(msg->getSock(), StC_UNSUCC_SIN_IN_ALRD_CONCT);
	}
	else
	{
		user = new User(msg->getValues()[0], msg->getSock());
		_connectedUsers.insert(std::pair<SOCKET, User*>(msg->getSock(), user));
		user->send(StC_SUCC_SIN_IN);
	}

	return user;
}

void TriviaServer::handleSignOut(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		_connectedUsers.erase(msg->getUser()->getSocket());
	}

	handleCloseRoom(msg);
	handleLeaveRoom(msg);
	handleLeaveGame(msg);
}

void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
	stringstream sstr;
	vector<string> scores = _db.getBestScores();
	sstr << StC_BEST_SCOR << scores[0] << scores[1] << scores[2];
	msg->getUser()->send(sstr.str());
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * msg)
{
	stringstream sstr;
	sstr << StC_PERS_STAT << _db.getPersonalStatus(msg->getUser()->getUsername())[0];
	msg->getUser()->send(sstr.str());
}

void TriviaServer::safeDeleteUesr(RecievedMessage* msg)
{
	try
	{
		handleSignOut(msg);
		if (msg->getUser() && closesocket(msg->getUser()->getSocket()) == SOCKET_ERROR)
		{
			throw exception("problem");
		}
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}



}
void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		Game* game = msg->getUser()->getGame();
		if (msg->getUser()->leavGame() == true)
			delete game;
	}
}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	Game *game = nullptr;
	int roomID = msg->getUser()->getRoom()->getId();
	try
	{
		game = new Game(msg->getUser()->getRoom()->getUsers(), msg->getUser()->getRoom()->getQuestionsNo(), _db);
	}
	catch (exception e)
	{
		msg->getUser()->send(StC_QUE_SEN + '0');
		cout << e.what() << endl;
	}

	if (game)
	{
		game->sendFirstQuestion();
		_roomsList.erase(_roomsList.find(roomID));
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	Game *game = msg->getUser()->getGame();

	if (game != NULL)
	{
		if (game->handleAnswerFromUser(msg->getUser(), atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str())) == false)
		{
			delete game;
		}
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	vector <string> values = msg->getValues();

	if (user != nullptr && user->creatRoom(_roomldSequence + 1, values[0], atoi(values[1].c_str()), atoi(values[2].c_str()), atoi(values[3].c_str())))
	{
		_roomldSequence++;
		_roomsList.insert(pair<int, Room*>(_roomldSequence, user->getRoom()));
		return true;
	}
	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		if (msg->getUser()->getRoom())
		{
			msg->getUser()->leavRoom();
			return true;
		}
	}
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user)
	{
		Room* room = getRoomById(atoi(msg->getValues()[0].c_str()));
		if (room)
		{
			user->joinRoom(room);
			return true;
		}
		else
			user->send(StC_UNSUCC_JOIN_ROO_ELSE);
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	if (msg->getUser() && msg->getUser()->getRoom())
	{
		int id = msg->getUser()->getRoom()->getId();
		if (msg->getUser()->closeRoom() != -1)
		{
			_roomsList.erase(id);
			return true;
		}
	}
	return false;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	Room* room = getRoomById(atoi(msg->getValues()[0].c_str()));

	if (room != nullptr)
	{
		user->send(room->getUsersListMessage());
	}
}


void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	string Smsg = StC_ROO_LIST_SEN + Helper::getPaddedNumber(_roomsList.size(), 4);
	for (auto it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		Smsg += Helper::getPaddedNumber(it->second->getId(), 4);
		Smsg += Helper::getPaddedNumber(it->second->getName().length(), 2);
		Smsg += it->second->getName();
	}
	msg->getUser()->send(Smsg);
}




User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	auto it = _connectedUsers.find(client_socket);


	if (it == _connectedUsers.end())
	{
		return nullptr;
	}
	return it->second;
}

User* TriviaServer::getUserByName(string username)
{
	auto it = _connectedUsers.begin();
	if (it == _connectedUsers.end())
	{
		return nullptr;
	}
	if (it->second->getUsername() == username)
	{
		return it->second;
	}
	return nullptr;
}

Room* TriviaServer::getRoomById(int roomId)
{
	auto it = _roomsList.find(roomId);

	if (it == _roomsList.end())
	{
		return nullptr;
	}
	return it->second;
}



