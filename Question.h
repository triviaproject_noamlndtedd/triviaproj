#pragma once

#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Question
{
private:
	string _question;
	string	_answers[4];
	int _correctAnswerIndex;
	int	_id;

public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	//Getters
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getID();
};