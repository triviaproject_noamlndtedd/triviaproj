#pragma once
#include"Room.h"
#include <WinSock2.h>
#include <Windows.h>
#include <Ws2tcpip.h>
#include "Helper.h"
#include <iostream>
#include "Game.h"

using namespace std;
class Game;
class Room;
class User
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _socket;

public:
	//Basics
	User(string username, SOCKET sock);
	~User();

	//send a string using Helper::sendData
	void send(string message);

	//Getters
	string getUsername();
	SOCKET getSocket();

	//Game
	Game* getGame();
	void setGame(Game* gm);
	void clearGame();
	bool leavGame();

	//Room
	Room* getRoom();
	void clearRoom();
	bool creatRoom(int roomId, string RoomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leavRoom();
	int closeRoom();
};