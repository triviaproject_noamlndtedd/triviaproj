#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <queue>
#include "RecievedMessage.h"
#include <thread>
#include <mutex>
#include "User.h"
#include <condition_variable>
#include "Validator.h"
#include "Game.h"
#include "DataBase.h"
#include "Room.h"
#include <sstream>

#define PORT 8826

using namespace std;

class TriviaServer
{
private:

	SOCKET _socket;
	map <SOCKET, User*> _connectedUsers;
	DataBase _db;
	map <int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	queue <RecievedMessage*> _queRcvMessages;
	int _roomldSequence;
	condition_variable cv;

public:
	TriviaServer();
	~TriviaServer();
	void server();

private:
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUesr(RecievedMessage* msg);

	//Recieving Msg
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);
	void addRecievedMessage(RecievedMessage* msg);
	void handleRecivedMessages();

	//Getters
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);
	Room* getRoomById(int roomId);

	//Handlers
	User* handleSignIn(RecievedMessage* msg);
	bool handleSignUp(RecievedMessage* msg);
	void handleSignOut(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);
};

