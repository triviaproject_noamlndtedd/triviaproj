#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db), _currentTurnAnswers(0), _question_no(questionsNo), _currQuestionIndex(0)
{
	if (insertGameToDB())
	{
		initQuestionsFromDB();
	}
	else
	{
		throw exception("ERROR: failed inserting new game");
	}

	_players = players;

	for (int i = 0; i < _players.size(); i++)
	{
		_results.insert(pair<string, int>(_players[i]->getUsername(), 0));
		_players[i]->setGame(this);
	}

}

Game::~Game()
{
	for (int i = 0; i < _questions.size(); i++)
	{
		delete _questions[i];
	}

	_questions.clear();
	_players.clear();
	_results.clear();
}

void Game::sendQuestionToAllUsers()
{
	stringstream msg;
	string que = _questions[_currQuestionIndex]->getQuestion(), *ans = _questions[_currQuestionIndex]->getAnswers();
	_currentTurnAnswers = 0;


	if (que.length() > 0)
	{
		msg << StC_QUE_SEN << Helper::getPaddedNumber(que.length(), 3) << que << Helper::getPaddedNumber(ans[0].length(), 3) << ans[0] << Helper::getPaddedNumber(ans[1].length(), 3) << ans[1] << Helper::getPaddedNumber(ans[2].length(), 3) << ans[2] << Helper::getPaddedNumber(ans[3].length(), 3) << ans[3];
		for (int i = 0; i < _players.size(); i++)
		{
			try
			{
				_players[i]->send(msg.str());
			}
			catch (exception e)
			{
				cout << e.what() << endl;
			}
		}
	}
}

void Game::handleFinishGame()
{
	_db.updateGameStatus(_id);

	stringstream msg;

	msg << StC_END_GAM << _players.size();

	for (int i = 0; i < _players.size(); i++)
		msg << Helper::getPaddedNumber(_players[i]->getUsername().length(), 2) << _players[i]->getUsername() << Helper::getPaddedNumber(_results[_players[i]->getUsername()], 2);

	for (int i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->clearGame();
			_players[i]->send(msg.str());
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

bool Game::handleNextTurn()
{
	bool re = true;
	if (_players.size() > 0)
	{
		if (_currentTurnAnswers == _players.size())
		{
			if ((++_currQuestionIndex) < _question_no)
				sendQuestionToAllUsers();
			else
			{
				re = false;
				handleFinishGame();
			}
		}
	}
	else
	{
		re = false;
		handleFinishGame();
	}
	return re;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool isCorrect = false;
	stringstream msg;

	_currentTurnAnswers++;
	msg << StC_SERV_ANS;


	if (--answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex())
	{
		_results[user->getUsername()] = _results[user->getUsername()] + 1;
		isCorrect = true;
	}


	try
	{
		_db.addAnswerToPlayer(_id, user->getUsername(), _questions[_currQuestionIndex]->getID(), (answerNo == 4 ? "" : _questions[_currQuestionIndex]->getAnswers()[answerNo]), isCorrect, time);
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}

	msg << (isCorrect ? 1 : 0);
	user->send(msg.str());

	return handleNextTurn();
}

bool Game::leaveGame(User* currUser)
{
	vector<User*>::iterator i;

	for (i = _players.begin(); i < _players.end() && *i != currUser; i++);
	_players.erase(i);
	return handleNextTurn();
}

int Game::getID()
{
	return _id;
}

bool Game::insertGameToDB()
{
	_id = _db.insertNewGame();
	return _id != -1;
}

void Game::initQuestionsFromDB()
{
	_questions = _db.initQuestions(_question_no);
}
