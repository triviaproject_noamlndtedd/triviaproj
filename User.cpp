#include "User.h"

User::User(string username, SOCKET sock) : _username(username), _socket(sock), _currRoom(nullptr), _currGame(nullptr) {}

User::~User() {}

void User::send(string message)
{
	if (message.length() > 0)
	{
		try
		{
			Helper::sendData(_socket, message);
		}
		catch (std::exception e)
		{
			cout << e.what() << endl;
		}
	}
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _socket;
}

Room * User::getRoom()
{
	return _currRoom;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::creatRoom(int roomId, string RoomName, int maxUsers, int questionsNo, int questionTime)
{
	bool boo = _currRoom == nullptr;
	string msg = StC_SUCC_CRE_ROO;
	if (boo)
	{
		_currRoom = new Room(roomId, this, RoomName, maxUsers, questionsNo, questionTime);
	}
	else
	{
		msg = StC_UNSUCC_CRE_ROO;
	}
	send(msg);
	return boo;
}

bool User::joinRoom(Room * newRoom)
{
	bool boo = false;
	if (_currRoom == nullptr && newRoom->joinRoom(this))
	{
		boo = true;
		_currRoom = newRoom;
	}
	return boo;
}

void User::leavRoom()
{
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int boo = -1;
	if (_currRoom != nullptr)
	{
		boo = _currRoom->closeRoom(this);
		if (boo != -1)
			clearRoom();
	}
	return boo;
}

void User::setGame(Game* gm)
{
	_currRoom = nullptr;
	_currGame = gm;
}

Game* User::getGame()
{
	return _currGame;
}

void User::clearGame()
{
	_currGame = nullptr;
}

bool User::leavGame()
{
	bool boo = false;
	if (_currGame != nullptr)
	{
		boo = _currGame->leaveGame(this);
		_currGame = nullptr;
	}
	return boo;
}
