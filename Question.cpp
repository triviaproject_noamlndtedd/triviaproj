#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4) : _id(id), _question(question)
{
	vector<string> ques;
	ques.push_back(correctAnswer);
	ques.push_back(answer2);
	ques.push_back(answer3);
	ques.push_back(answer4);
	random_shuffle(ques.begin(), ques.end());
	for (int i = 0; i < ques.size(); i++)
	{
		_answers[i] = ques[i];
		if (_answers[i] == correctAnswer)
		{
			_correctAnswerIndex = i;
		}
	}
}

string Question::getQuestion()
{
	return _question;
}

string * Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getID()
{
	return _id;
}
