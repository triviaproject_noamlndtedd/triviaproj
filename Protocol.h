#pragma once

/*****************************
*	Rules for protocol:		 *
*	StC - Server 2 Client	 *
*	SUCC - Successfully		 *
*	UNSUCC - Unsuccessfull	 *
*	SEN - sender[need an add]*		
*	SIN - Sign				 *
*	ILG - illegal			 *
*	ROO - Room				 *
*	GAM - Game				 *
*	CRE - Create			 *
*	LEAV - Leave			 *
*****************************/

//StC
	//Sign in
		#define StC_SUCC_SIN_IN "1020"
		#define StC_UNSUCC_SIN_IN_WRNG_DTLS "1021"
		#define StC_UNSUCC_SIN_IN_ALRD_CONCT "1022"
	//Sign up
		#define StC_SUCC_SIN_UP "1040"
		#define StC_UNSUCC_SIN_UP_PASS_ILG "1041"
		#define StC_UNSUCC_SIN_UP_USERN_EXIS "1042"
		#define StC_UNSUCC_SIN_UP_USERN_ILG "1043"
		#define StC_UNSUCC_SIN_UP_ELSE "1044"
	//Rooms list
		#define StC_ROO_LIST_SEN "106"//Need an add
	//Users list in Room
		#define StC_USERS_LIST_SEN "108"//Need an add
	//Join Room
		#define StC_SUCC_JOIN_ROO "1100"
		#define StC_UNSUCC_JOIN_ROO_IS_FULL "1101"
		#define StC_UNSUCC_JOIN_ROO_ELSE "1102"
	//Leave Room
		#define StC_SUCC_LEAV_ROO "1120"
	//Create Room
		#define StC_SUCC_CRE_ROO "1140"
		#define StC_UNSUCC_CRE_ROO "1141"
	//Close Room
		#define StC_CLOS_ROO "116"
	//Send Question
		#define StC_QUE_SEN "118"//Need an add
		#define StC_SERV_ANS "120"
		#define StC_END_GAM "121"
	//Personal
		#define StC_BEST_SCOR "124"
		#define StC_PERS_STAT "126"


//CtS
		#define	CtS_SIN_IN 200
		#define	CtS_SIN_OUT 201
		#define	CtS_SIN_UP 203
		#define	CtS_GET_EXIS_ROOMS 205
		#define	CtS_USERS_IN_ROO 207
		#define	CtS_JOIN_EXIS_ROO 209
		#define	CtS_LEAV_ROO 211
		#define	CtS_CRE_NEW_ROO 213
		#define	CtS_CLOS_ROO 215
		#define	CtS_START_GAM 217
		#define	CtS_ANS 219
		#define	CtS_LEAV_GAM 222
		#define	CtS_BEST_SCORE 223
		#define	CtS_STAT 225
		#define CtS_USER_EXIT 299
